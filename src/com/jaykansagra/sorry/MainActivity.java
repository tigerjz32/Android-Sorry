package com.jaykansagra.sorry;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	private Button normal, spicy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		normal = (Button) findViewById(R.id.Normal);
		normal.setOnClickListener(this);

		spicy = (Button) findViewById(R.id.Spicy);
		spicy.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.Normal: normal(); break;
			case R.id.Spicy: spicy(); break;
		}
	}
	
	private void normal() {
		startActivity(new Intent("com.jaykansagra.sorry.Normal"));
	}
	
	private void spicy() {
		startActivity(new Intent("com.jaykansagra.sorry.Spicy"));
	}
}
